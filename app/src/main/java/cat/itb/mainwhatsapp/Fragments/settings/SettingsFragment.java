package cat.itb.mainwhatsapp.Fragments.settings;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.appbar.MaterialToolbar;

import cat.itb.mainwhatsapp.R;

public class SettingsFragment extends Fragment {

    LinearLayout linearLayout_settings_account;
    LinearLayout linearLayout_settings_chats;
    LinearLayout linearLayout_settings_notifications;
    LinearLayout linearLayout_settings_dataAndStorage;
    LinearLayout linearLayout_settings_help;
    LinearLayout linearLayout_settings_inviteAFriend;
    LinearLayout linearLayout_settings_profile;

    MaterialToolbar settings_toolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        settings_toolbar = view.findViewById(R.id.toolbar_settings);

        settings_toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);

        settings_toolbar.setNavigationOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_fragmentHome));

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        linearLayout_settings_account = view.findViewById(R.id.layout_settings_account);
        linearLayout_settings_chats = view.findViewById(R.id.layout_settings_chats);
        linearLayout_settings_notifications = view.findViewById(R.id.layout_settings_notifications);
        linearLayout_settings_dataAndStorage = view.findViewById(R.id.layout_settings_storage);
        linearLayout_settings_help = view.findViewById(R.id.layout_settings_help);
        linearLayout_settings_inviteAFriend = view.findViewById(R.id.layout_settings_invite);
        linearLayout_settings_profile = view.findViewById(R.id.layout_profile);

        linearLayout_settings_profile.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_settingsProfileFragment));

        linearLayout_settings_account.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_settingsAccountFragment));

        linearLayout_settings_chats.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_settingsChatsFragment));

        linearLayout_settings_notifications.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_settingsNotificationsFragment));

        linearLayout_settings_dataAndStorage.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_settingsDataFragment));

        linearLayout_settings_help.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_settingsFragment_to_settingsHelpFragment));

        linearLayout_settings_inviteAFriend.setOnClickListener(v -> {

        });
    }

}