package cat.itb.mainwhatsapp.Fragments.settings;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.appbar.MaterialToolbar;

import cat.itb.mainwhatsapp.R;

public class SettingsProfilePhotoFragment extends Fragment {

    MaterialToolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings_profile_photo, container, false);

        toolbar = v.findViewById(R.id.toolbar_settings_profile_photo);

        toolbar.setNavigationOnClickListener(v1 -> Navigation.findNavController(v1).navigate(R.id.action_settingsProfilePhotoFragment_to_settingsProfileFragment));

        return v;
    }
}