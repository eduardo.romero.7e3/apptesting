package cat.itb.mainwhatsapp.Fragments.main;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import cat.itb.mainwhatsapp.Adapter.MainContactsAdapter;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;


public class RecyclerChatFragment extends Fragment {

    RecyclerView recyclerView;
    

    public RecyclerChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<User> userList = new ArrayList<User>();

        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));
        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));
        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));
        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));

        Log.d("hello", userList.toString());

        recyclerView = view.findViewById(R.id.recyclerviewContactsChat);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new MainContactsAdapter(userList, getContext()));

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycler_chat, container, false);
    }
}