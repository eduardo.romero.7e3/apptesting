package cat.itb.mainwhatsapp.Fragments.main;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import cat.itb.mainwhatsapp.Adapter.EstadoAdapter;
import cat.itb.mainwhatsapp.Adapter.MainContactsAdapter;
import cat.itb.mainwhatsapp.Model.Estado;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;


public class RecyclerStatusFragment extends Fragment {

    RecyclerView recyclerView;

    public RecyclerStatusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycler_status, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Estado> estados = new ArrayList<Estado>();

        estados.add(new Estado("1", "Random Rodelas", "10:30", BitmapFactory.decodeResource(getResources(), R.mipmap.fondo1), BitmapFactory.decodeResource(getResources(), R.mipmap.p2)));
        estados.add(new Estado("2", "Homer Simpson", "11:30", BitmapFactory.decodeResource(getResources(), R.mipmap.fondo2), BitmapFactory.decodeResource(getResources(), R.mipmap.p1)));
        estados.add(new Estado("3", "Leonel Messi", "12:30", BitmapFactory.decodeResource(getResources(), R.mipmap.fondo3), BitmapFactory.decodeResource(getResources(), R.mipmap.p3)));


        recyclerView = view.findViewById(R.id.recyclerViewEstadoFragment);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new EstadoAdapter(estados));

    }
}