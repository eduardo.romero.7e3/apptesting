package cat.itb.mainwhatsapp.Fragments.main;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import cat.itb.mainwhatsapp.Adapter.MainCallsAdapter;
import cat.itb.mainwhatsapp.Adapter.MainContactsAdapter;
import cat.itb.mainwhatsapp.Model.Call;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;

public class RecyclerCallsFragment extends Fragment {

    RecyclerView recyclerView;

    public RecyclerCallsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycler_calls, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Call> calls = new ArrayList<Call>();

        calls.add(new Call("Random Rodelas", true, BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "Normal", "27 de febrero 09:00"));
        calls.add(new Call("Homer Simpson", false, BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "Normal", "10 de febrero 10:00"));
        calls.add(new Call("Leonel Messi", true, BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "Video", "09 de febrero 22:00"));
        calls.add(new Call("Random Rodelas", false, BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "Normal", "27 de enero 09:00"));
        calls.add(new Call("Homer Simpson", true, BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "Video", "10 de enero 10:00"));
        calls.add(new Call("Leonel Messi", true, BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "Video", "09 de enero 22:00"));
        calls.add(new Call("Random Rodelas", false, BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "Normal", "12/12/20 09:00"));
        calls.add(new Call("Homer Simpson", true, BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "Normal", "22/11/20 10:00"));
        calls.add(new Call("Leonel Messi", true, BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "Video", "19/11/20 22:00"));

        recyclerView = view.findViewById(R.id.recyclerViewCallsFragment);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new MainCallsAdapter(calls, getContext()));
    }
}