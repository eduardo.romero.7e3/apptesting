package cat.itb.mainwhatsapp.Fragments.chat;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;

public class FragmentChatProfile extends Fragment {

    ImageView profilePicView;
    TextView profileNameView;
    User user;

    public FragmentChatProfile() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_chats_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profilePicView = view.findViewById(R.id.profile_pic_view);
        profileNameView = view.findViewById(R.id.profileNameTextView);

        if (getArguments() != null) {
            user = getArguments().getParcelable("userToProfile");
            profilePicView.setImageBitmap(user.getImage());
            profileNameView.setText(user.getName());
            profileNameView.setTextColor(Color.WHITE);
            profileNameView.setShadowLayer(2.6f, 2.5f, 2.3f, Color.BLACK);
        }
    }
}
