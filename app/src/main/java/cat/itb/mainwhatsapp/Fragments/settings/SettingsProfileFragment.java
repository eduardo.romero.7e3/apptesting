package cat.itb.mainwhatsapp.Fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import cat.itb.mainwhatsapp.R;

public class SettingsProfileFragment extends Fragment {

    MaterialToolbar toolbar;
    FloatingActionButton fab_camera;
    BottomSheetDialog bottomSheetCamera;
    BottomSheetDialog bottomSheetName;
    BottomSheetDialog bottomSheetStatus;
    BottomSheetDialog bottomSheetNumber;

    LinearLayout edit_name;
    LinearLayout edit_status;
    LinearLayout edit_number;
    MaterialButton cancel_button_edit_name;
    MaterialButton cancel_button_edit_status;
    MaterialButton cancel_button_edit_number;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings_profile, container, false);

        toolbar = v.findViewById(R.id.toolbar_settings_profile);

        toolbar.setNavigationOnClickListener(v1 -> Navigation.findNavController(v1).navigate(R.id.action_settingsProfileFragment_to_settingsFragment));

        bottomSheetCamera = new BottomSheetDialog(requireContext());

        bottomSheetName = new BottomSheetDialog(requireContext());

        bottomSheetStatus = new BottomSheetDialog(requireContext());

        bottomSheetNumber = new BottomSheetDialog(requireContext());

        fab_camera= v.findViewById(R.id.fab_settings_fragment_profile);

        fab_camera.setOnClickListener(v12 -> showBottomDialogPick());

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edit_name = view.findViewById(R.id.layout_settings_profile_editName);

        edit_name.setOnClickListener(v1 -> showBottomDialogEditName());

        edit_status = view.findViewById(R.id.layout_settings_profile_editStatus);

        edit_status.setOnClickListener(v -> showBottomDialogEditStatus());

        edit_number = view.findViewById(R.id.layout_settings_profile_editNumber);

        edit_number.setOnClickListener(v -> showBottomDialogEditNumber());


    }

    private void showBottomDialogEditNumber() {
        View view = getLayoutInflater().inflate(R.layout.settings_profile_edit_number, null);

        bottomSheetNumber.setContentView(view);

        Objects.requireNonNull(bottomSheetNumber.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        bottomSheetNumber.show();

        bottomSheetNumber.setOnDismissListener(dialog -> bottomSheetNumber.cancel());

        cancel_button_edit_number = view.findViewById(R.id.button_profile_edit_number_cancel);

        cancel_button_edit_number.setOnClickListener(v -> bottomSheetNumber.cancel());
    }

    private void showBottomDialogEditStatus() {
        View view = getLayoutInflater().inflate(R.layout.settings_profile_edit_status, null);

        bottomSheetStatus.setContentView(view);

        Objects.requireNonNull(bottomSheetStatus.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        bottomSheetStatus.show();

        bottomSheetStatus.setOnDismissListener(dialog -> bottomSheetStatus.cancel());

        cancel_button_edit_status = view.findViewById(R.id.button_profile_edit_status_cancel);

        cancel_button_edit_status.setOnClickListener(v -> bottomSheetStatus.cancel());
    }

    private void showBottomDialogEditName() {

        View view = getLayoutInflater().inflate(R.layout.settings_profile_edit_name, null);

        bottomSheetName.setContentView(view);

        Objects.requireNonNull(bottomSheetName.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        bottomSheetName.show();

        bottomSheetName.setOnDismissListener(dialog -> bottomSheetName.cancel());

        cancel_button_edit_name = view.findViewById(R.id.button_profile_editName_cancel);

        cancel_button_edit_name.setOnClickListener(v -> bottomSheetName.cancel());
    }

    private void showBottomDialogPick() {
        View view = getLayoutInflater().inflate(R.layout.settings_profile_camera_options, null);

        bottomSheetCamera.setContentView(view);

        Objects.requireNonNull(bottomSheetCamera.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        bottomSheetCamera.show();

        bottomSheetCamera.setOnDismissListener(dialog -> bottomSheetCamera.cancel());
    }
}