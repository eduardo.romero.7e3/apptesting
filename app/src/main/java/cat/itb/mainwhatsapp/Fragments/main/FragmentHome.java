package cat.itb.mainwhatsapp.Fragments.main;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.lang.reflect.Field;

import cat.itb.mainwhatsapp.Adapter.ViewPagerAdapter;
import cat.itb.mainwhatsapp.R;

public class FragmentHome extends Fragment{


    TabLayout tabLayout;
    ViewPager2 viewPager2;
    MaterialToolbar toolbarMain, searchToolbar;
    final String[] NAME_TABS = {"", "Chats", "Status", "Calls"};
    FloatingActionButton floatingActionButton, floatingActionButtonMini;

    SearchView searchView;

    Menu search_menu;
    MenuItem item_search;


    public FragmentHome() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = view.findViewById(R.id.tabLayoutMain);
        viewPager2 = view.findViewById(R.id.viewPagerMain);
        toolbarMain = view.findViewById(R.id.topAppBar);
        searchToolbar = view.findViewById(R.id.topAppBarSearch);
        floatingActionButton = view.findViewById(R.id.floating_action_button);
        floatingActionButtonMini = view.findViewById(R.id.floating_action_button_mini);

        setupTabLayout();
        setSearchToolbar();

        toolbarMain.setOnMenuItemClickListener(new androidx.appcompat.widget.Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.search_main_toolbar:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            searchView.requestFocus();
                            circleReveal(R.id.topAppBarSearch, 1, true, true);
                        }else {
                            searchToolbar.setVisibility(View.GONE);
                        }
                        return true;
                    case R.id.options:
                        NavDirections navDirections = FragmentHomeDirections.actionFragmentHomeToSettingsFragment();
                        NavHostFragment.findNavController(getParentFragment()).navigate(navDirections);
                    default:
                        return false;
                }
            }
        });

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position){
                    case 1:
                        floatingActionButton.setVisibility(View.VISIBLE);
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_comment));
                        floatingActionButtonMini.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        floatingActionButton.setVisibility(View.VISIBLE);
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera));
                        floatingActionButtonMini.setVisibility(View.VISIBLE);
                        floatingActionButtonMini.setImageDrawable(getResources().getDrawable(R.drawable.ic_pen_edit));
                        break;
                    case 3:
                        floatingActionButton.setVisibility(View.VISIBLE);
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_call));
                        floatingActionButtonMini.setVisibility(View.VISIBLE);
                        floatingActionButtonMini.setImageDrawable(getResources().getDrawable(R.drawable.ic_videocall));
                        break;
                }
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections = FragmentHomeDirections.actionFragmentHomeToContactFragment();
                Navigation.findNavController(v).navigate(navDirections);
            }
        });

    }

    /**
     *
     */
    private void setupTabLayout(){

        viewPager2.setAdapter(new ViewPagerAdapter(getActivity()));

        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText( NAME_TABS[position] );
                if(position == 0){
                    tab.setIcon(R.drawable.ic_camera);
                }
                viewPager2.setCurrentItem(tab.getPosition(), true);
            }
        }).attach();


        LinearLayout layout = ((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(0));
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) layout.getLayoutParams();
        layoutParams.weight = 0.4f;
        layout.setLayoutParams(layoutParams);


    }

    private void setSearchToolbar(){
        if (searchToolbar != null){
            searchToolbar.inflateMenu(R.menu.menu_toolbar_search);
            search_menu = searchToolbar.getMenu();

            searchToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.topAppBarSearch, 1, true, false);
                    }else {
                        searchToolbar.setVisibility(View.GONE);
                    }
                }
            });
        }

        item_search = search_menu.findItem(R.id.actionFilterSearch);

        MenuItemCompat.setOnActionExpandListener(item_search, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circleReveal(R.id.topAppBarSearch,1,true,false);
                }
                else
                    searchToolbar.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });

        initSearchView();

    }

    private void initSearchView(){
        searchView = (SearchView) search_menu.findItem(R.id.actionFilterSearch).getActionView();

        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) searchView.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));

        magImage.setVisibility(View.GONE);
        searchView.setQueryHint("Serach...");
        searchView.setIconifiedByDefault(false);

        try {
            Field mDrawable = SearchView.class.getDeclaredField("mIcon");
            mDrawable.setAccessible(true);
            Drawable drawable = (Drawable) mDrawable.get(searchView);
            drawable.setVisible(false,false);
            Log.d("prova", "si funciona ");
        } catch (Exception e) {
            Log.d("prova", "no funciona ");
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_home, container, false);
        return v;
    }

    @SuppressLint("PrivateResource")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void circleReveal(int viewId, int posFromRight, boolean containsOverFlow, final boolean isShow){
        final View view = getView().findViewById(viewId);

        int width=view.getWidth();

        if(posFromRight>0){
            width-=(posFromRight*getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2);
        }
        if(containsOverFlow){
            width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);
        }

        int cx = width;
        int cy = view.getHeight()/2;

        Animator anim;

        if(isShow){
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, (float)width);
        }else {
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, (float)width, 0);
        }

        anim.setDuration((long)400);

        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow){
                    view.setVisibility(View.GONE);
                    toolbarMain.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        if(isShow){
            toolbarMain.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        }

        anim.start();
    }
}