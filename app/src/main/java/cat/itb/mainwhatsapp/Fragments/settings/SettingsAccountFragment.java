package cat.itb.mainwhatsapp.Fragments.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.appbar.MaterialToolbar;

import cat.itb.mainwhatsapp.R;


public class SettingsAccountFragment extends Fragment {

    MaterialToolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings_account, container, false);

        toolbar = v.findViewById(R.id.toolbar_settings_account);

        toolbar.setNavigationOnClickListener(v1 -> Navigation.findNavController(v1).navigate(R.id.action_settingsAccountFragment_to_settingsFragment));

        return v;
    }
}