package cat.itb.mainwhatsapp.Model;

import java.util.List;

public class Chat {
    private String id;
    private String user1id;
    private String user2id;
    private List<Message> conversation;

    public Chat(String user1id, String user2id, List<Message> conversation) {
        this.user1id = user1id;
        this.user2id = user2id;
        this.conversation = conversation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser1id() {
        return user1id;
    }

    public void setUser1id(String user1id) {
        this.user1id = user1id;
    }

    public String getUser2id() {
        return user2id;
    }

    public void setUser2id(String user2id) {
        this.user2id = user2id;
    }

    public List<Message> getConversation() {
        return conversation;
    }

    public void setConversation(List<Message> conversation) {
        this.conversation = conversation;
    }
}
