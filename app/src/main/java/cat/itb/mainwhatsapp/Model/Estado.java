package cat.itb.mainwhatsapp.Model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Estado implements Parcelable {
    private String id;
    private String userid;
    private String time;
    private Bitmap imatge;
    private Bitmap userImage;

    public Estado(String id, String userid, String temps, Bitmap imatge, Bitmap userImage) {
        this.id = id;
        this.userid = userid;
        this.time = temps;
        this.imatge = imatge;
        this.userImage = userImage;
    }

    protected Estado(Parcel in) {
        id = in.readString();
        userid = in.readString();
        time = in.readString();
        imatge = in.readParcelable(Bitmap.class.getClassLoader());
        userImage = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<Estado> CREATOR = new Creator<Estado>() {
        @Override
        public Estado createFromParcel(Parcel in) {
            return new Estado(in);
        }

        @Override
        public Estado[] newArray(int size) {
            return new Estado[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Bitmap getImatge() {
        return imatge;
    }

    public void setImatge(Bitmap imatge) {
        this.imatge = imatge;
    }

    public Bitmap getUserImage() {
        return userImage;
    }

    public void setUserImage(Bitmap userImage) {
        this.userImage = userImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userid);
        dest.writeString(time);
        dest.writeValue(imatge);
        dest.writeValue(userImage);
    }
}
