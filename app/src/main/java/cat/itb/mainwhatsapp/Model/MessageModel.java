package cat.itb.mainwhatsapp.Model;

public class MessageModel {
    private String user;
    private String message;
    private String timestamp;
    private boolean you;

    public MessageModel(String user, String message, String timestamp, boolean you) {
        this.user = user;
        this.message = message;
        this.timestamp = timestamp;
        this.you = you;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean setIsYou(String name) {
        return you;
    }

    public boolean isYou() {
        return you;
    }

    public void setYou(boolean b) {
        this.you = b;
    }
}
