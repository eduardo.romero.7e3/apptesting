package cat.itb.mainwhatsapp.Adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;

import cat.itb.mainwhatsapp.Model.Call;
import cat.itb.mainwhatsapp.R;

public class MainCallsAdapter extends RecyclerView.Adapter<MainCallsAdapter.CallViewHolder> {
    List<Call> calls;
    Context context;

    public MainCallsAdapter(List<Call> calls, Context context) {
        this.calls = calls;
        this.context = context;
    }

    @NonNull
    @Override
    public CallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calls, parent, false);
        return new CallViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull CallViewHolder holder, int position) {
        holder.bindDate(calls.get(position));
    }

    @Override
    public int getItemCount() {
        return calls.size();
    }

    class CallViewHolder extends RecyclerView.ViewHolder{

        ShapeableImageView imageViewProfile, typeCall, sendCall;
        TextView name, time;

        public CallViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewProfile = itemView.findViewById(R.id.imageProfileCallsItemRecyclerView);
            typeCall = itemView.findViewById(R.id.imageTypeCallsItemRecyclerView);
            sendCall = itemView.findViewById(R.id.imageSendCallsItemRecyclerView);
            name = itemView.findViewById(R.id.textViewNameCallsPerfilRecyclerView);
            time = itemView.findViewById(R.id.textViewTimeCallsPerfilRecyclerView);

        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        private void bindDate(Call c){

            imageViewProfile.setImageBitmap(c.getImage());

            if(c.getType().equals("Normal")){
                typeCall.setImageDrawable(context.getDrawable(R.drawable.ic_call));
            }else if(c.getType().equals("Video")){
                typeCall.setImageDrawable(context.getDrawable(R.drawable.ic_videocall));
            }

            if(c.isSend()){
                sendCall.setImageDrawable(context.getDrawable(R.drawable.ic_filled_arrow_back));
                sendCall.setColorFilter(context.getColor(R.color.colorAccent));
            }else{
                sendCall.setImageDrawable(context.getDrawable(R.drawable.ic_filled_arrow_forward));
                sendCall.setColorFilter(context.getColor(R.color.red));
            }

            name.setText(c.getName());
            time.setText(c.getTime());
        }
    }


}
