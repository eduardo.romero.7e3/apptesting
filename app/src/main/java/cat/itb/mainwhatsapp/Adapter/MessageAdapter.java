package cat.itb.mainwhatsapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.mainwhatsapp.Model.MessageModel;
import cat.itb.mainwhatsapp.R;

public class MessageAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    static List<MessageModel> list;

    public MessageAdapter(List<MessageModel> list) {
        MessageAdapter.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        MessageModel message = (MessageModel) list.get(position);
        if (message.isYou()) return VIEW_TYPE_MESSAGE_SENT;
        else return VIEW_TYPE_MESSAGE_RECEIVED;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            v = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_chat_outgoing_message, parent, false);
            return new OutgoingHolder(v);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            v = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_chat_incoming_message, parent, false);
            return new IncomingHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageModel message = (MessageModel) list.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((OutgoingHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((IncomingHolder) holder).bind(message);
                break;
        }
    }

    public void setMessageList(List<MessageModel> list) {
        MessageAdapter.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class IncomingHolder extends RecyclerView.ViewHolder {

        TextView messageView, timestampView;

        public IncomingHolder(@NonNull View itemView) {
            super(itemView);

            messageView = itemView.findViewById(R.id.item_textview_chat_incmessage);
            timestampView = itemView.findViewById(R.id.item_textview_chat_inctimestamp);

        }

        void bind(MessageModel message) {
            messageView.setText(message.getMessage());
            timestampView.setText(message.getTimestamp());
        }
    }

    public static class OutgoingHolder extends RecyclerView.ViewHolder {

        TextView messageView, timestampView;
        ImageView statusView;

        public OutgoingHolder(@NonNull View itemView) {
            super(itemView);

            messageView = itemView.findViewById(R.id.item_textview_chat_outmessage);
            timestampView = itemView.findViewById(R.id.item_textview_chat_outtimestamp);
            statusView = itemView.findViewById(R.id.item_textview_chat_status);
        }

        void bind(MessageModel message) {
            messageView.setText(message.getMessage());
            timestampView.setText(message.getTimestamp());
        }
    }
}
