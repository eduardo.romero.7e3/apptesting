package cat.itb.mainwhatsapp.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import cat.itb.mainwhatsapp.Fragments.main.CameraFragment;
import cat.itb.mainwhatsapp.Fragments.main.RecyclerCallsFragment;
import cat.itb.mainwhatsapp.Fragments.main.RecyclerChatFragment;
import cat.itb.mainwhatsapp.Fragments.main.RecyclerStatusFragment;

public class ViewPagerAdapter extends FragmentStateAdapter {

    private static final int CARD_ITEM_SIZE = 4;

    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        Fragment fragment = new Fragment();

        System.out.println("hola3333333333333333333333333333333333333333 " + position);

        switch (position){
            case 0:
                fragment = new CameraFragment();
                break;
            case 1:
                fragment = new RecyclerChatFragment();
                break;
            case 2:
                fragment = new RecyclerStatusFragment();
                break;
            case 3:
                fragment = new RecyclerCallsFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getItemCount() {
        return CARD_ITEM_SIZE;
    }
}
