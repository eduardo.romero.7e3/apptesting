package cat.itb.mainwhatsapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;

import cat.itb.mainwhatsapp.Fragments.main.FragmentHomeDirections;
import cat.itb.mainwhatsapp.Model.Estado;
import cat.itb.mainwhatsapp.R;

public class EstadoAdapter extends RecyclerView.Adapter<EstadoAdapter.EstadoHolder> {

    List<Estado> estados;

    public EstadoAdapter(List<Estado> estados) {
        this.estados = estados;
    }

    @NonNull
    @Override
    public EstadoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_status, parent, false);
        return new EstadoHolder(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull EstadoHolder holder, int position) {
        holder.bindData(estados.get(position));

    }

    @Override
    public int getItemCount() {
        return estados.size();
    }

    class EstadoHolder extends RecyclerView.ViewHolder{

        ShapeableImageView perfilImage;
        TextView user, time;

        public EstadoHolder(@NonNull View itemView) {
            super(itemView);
            user = itemView.findViewById(R.id.textViewNameEstadoRecyclerView);
            time = itemView.findViewById(R.id.textViewTimeEstadoRecyclerView);
            perfilImage = itemView.findViewById(R.id.imageProfileEstadoItemRecyclerView);

            itemView.setOnClickListener(v -> {
                NavDirections navDirections = FragmentHomeDirections.actionFragmentHomeToEstadoFragment2(estados.get(getAdapterPosition()));
                Navigation.findNavController(v).navigate(navDirections);
            });

        }

        public void bindData(Estado estado){
            perfilImage.setImageBitmap(estado.getUserImage());
            user.setText(estado.getUserid());
            time.setText(estado.getTime());
        }


    }
}
